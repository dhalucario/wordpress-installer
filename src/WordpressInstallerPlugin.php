<?php

namespace Dhalucario\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class WordpressInstallerPlugin implements PluginInterface {

    public function activate(Composer $composer, IOInterface $io) {
        $installer = new WordpressInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }

    public function deactivate(Composer $composer, IOInterface $io) {
        // TODO: Implement deactivate() method.
    }

    public function uninstall(Composer $composer, IOInterface $io) {
        // TODO: Implement uninstall() method.
    }

}
