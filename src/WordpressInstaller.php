<?php

namespace Dhalucario\Composer;

use Composer\Composer;
use Composer\Installer\InstallerInterface;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use JsonSchema\Exception\InvalidConfigException;

class WordpressInstaller implements InstallerInterface
{

    private $io;
    private $composer;

    private $wordpressBaseUrl = 'https://wordpress.org/';

    public function __construct(IOInterface $io, Composer $composer)
    {
        $this->io = $io;
        $this->composer = $composer;
    }

    public function supports($packageType)
    {
        // TODO: Implement supports() method.
    }

    public function isInstalled(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        // TODO: Implement isInstalled() method.
    }

    public function download(PackageInterface $package, PackageInterface $prevPackage = null)
    {
        // TODO: Implement download() method.
    }

    public function prepare($type, PackageInterface $package, PackageInterface $prevPackage = null)
    {
        $tempPath = $this->getTempPath($package);
        mkdir($tempPath, 0x755, true);
    }

    public function getTempPath(PackageInterface $package) {
        $installPath = $this->getInstallPath($package);
        return $installPath . PATH_SEPARATOR . 'tmp';
    }

    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $config = $this->composer->getConfig();
        $extras = $config->get('extras');
        $lang = '';

        if (array_key_exists('wordpress-install-lang', $extras)) {
            $lang = '-' . $extras['wordpress-install-lang'];
        }

        $this->downloadWordpress($version, $lang, $package);
    }

    public function downloadWordpress($version, $lang, PackageInterface $package) {
        $curl = curl_init();
        $targetPath = $this->getTempPath($package);

        $splitLang = explode('_', $lang);
        if (count($splitLang) !== 2 || count($splitLang[0]) !== 2 || count($splitLang[0]) !== 2) {
            throw new InvalidConfigException('wordpress-install-lang is not a valid language string');
        }

        $filename = 'wordpress-' . $version . $lang . '.zip';
        $downloadFilePath = $targetPath . PATH_SEPARATOR . $filename;
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->wordpressBaseUrl . PATH_SEPARATOR . $filename,
            CURLOPT_FILE => $downloadFilePath,
            CURLOPT_HTTPGET => true
        ]);

        $curl = curl_exec($curl);

        return $downloadFilePath;
    }

    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        // TODO: Implement update() method.
    }

    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        // TODO: Implement uninstall() method.
    }

    public function cleanup($type, PackageInterface $package, PackageInterface $prevPackage = null)
    {
        // TODO: Implement cleanup() method.
    }

    public function getInstallPath(PackageInterface $package)
    {
        $config = $this->composer->getConfig();
        $extras = $config->get('extras');

        if (!array_key_exists('wordpress-install-dir', $extras)) {
            throw new InvalidConfigException('Unable to find wordpress-install-dir in extras');
        }

        $installPath = realpath($extras['wordpress-install-dir']);

        if ($installPath === false) {
            throw new InvalidConfigException('wordpress-install-dir is not a path');
        }

        return $installPath;
    }
}
