# The Soft License 1.0

Copyright (c) 2020 Leon Grünewald

## Definitions

You = The end user
Me = The author of this software

## Terms and conditions for distribution, copying or modification

1. You may use the software (source code, documentation or compiled binary if applicable) in your own private and
commercially free of charge, under the following conditions:
a. You must not resell modified versions of the software without explicit written permission from the author.
b. You may sell work hours to modify the software, but you may not sell the modified software as a product.

2. You may redistribute and use the unmodified software in any shape, way or form.

3. By modifying the source code or documentation of the software you grant the author access to the modifications
in a way which allows him!bn to integrate the modifications back into this software without any technical or legal restrictions,
especially if the software is public or commercially sold.

4. Neither the name of the author nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

## Liability
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
